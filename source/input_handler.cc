#include "input_handler.h"

void InputHandler::print_usage_message() {
	std::cout <<"Please use the command line to specify "
			"an input parameter file with a -p flag\n"
			"You may use the following structure for your "
			"parameter file\n";

	prm.print_parameters (std::cout, ParameterHandler::Text);
}

void InputHandler::declare_parameters() {

	prm.enter_subsection("Elasticity Parameters"); {
		prm.declare_entry("Lame's Parameter Mu", "80.77e+3",
				Patterns::Double(0, -1),
				"Lame's Second Parameter a.k.a Shear Modulus [MPa]");
		prm.declare_entry("Lame's Parameter Lambda", "121.15e+3",
				Patterns::Double(0, -1),
				"Lame's First Parameter [MPa]"); 
		prm.declare_entry("Critical Fracture Energy", "2.7",
				Patterns::Double(0, -1),
				"The elastic energy restitution rate for crack growth [N/mm]");
	}
	prm.leave_subsection();

	prm.enter_subsection("DG Parameters"); {
		prm.declare_entry("Face Term Penalty", "1.0e+14",
				Patterns::Double(0, -1),
				"The penalty parameter for face terms in DG");
		prm.declare_entry("Boundary Term Penalty", "1.0e+10",
				Patterns::Double(0, -1),
				"The penalty parameter for enforcing Dirichlet boundary conditions in DG");
		prm.declare_entry("sform", "1",
				Patterns::Integer(-1,1),
				"The sform determines which class of DG method will be used"
				"\n(-1:SIPG, 0:IIPG, +1:NIPG, OBB)"
                "\nNote: SIPG is unstable and sensitive to changes in Penalty");
        prm.declare_entry("Scale Penalty", "false",
                Patterns::Bool(),
                "true results in scaling of face penalty at internal edges");
	}
	prm.leave_subsection();

	prm.enter_subsection("Phase-Field Parameters"); {
			prm.declare_entry("Dynamic PF Parameters", "false",
                    Patterns::Bool(),
                    "true to enable auto-determination of kappa and epsilon from mesh size");
            prm.declare_entry("Constant K", "1e-12",
					Patterns::Double(0,1),
					"Energy regularization constant Kappa"
                    "\nCan theoretically be chosen to be zero");
			prm.declare_entry("Constant E", "4.4e-2",
					Patterns::Double(0, 1),
					"Space regularization constant Epsilon [mm]"
                    "\nControls width of cracked PF zone"
                    "\nShould be chosen ~ O(h)");
            prm.declare_entry("Linearization Scheme", "simple",
                    Patterns::Selection("simple|interpolation"),
                    "Specify how the semi-linear form is linearized"
                    "\nNote: interpolation is not stable with discontinuous methods");
	}
	prm.leave_subsection();

	prm.enter_subsection("Mesh Parameters"); {
			prm.declare_entry("Refine Apriori", "false",
					Patterns::Bool(),
					"true for a-priori refinement in the case of MS/MT tests");
			prm.declare_entry("Predictor Corrector", "true",
					Patterns::Bool(),
					"true/false to enable/disable P-C mesh adaptivity");
			prm.declare_entry("Threshold PF for Refinement", "0.8",
					Patterns::Double(0,1),
					"Value of PF for which a cell will be refined");
            prm.declare_entry("Num Global Refines", "3",
                    Patterns::Integer(2,-1),
                    "Number of global refines prior to local refinement");
	}
	prm.leave_subsection();
	
    prm.enter_subsection("Runtime Parameters"); {
            prm.declare_entry("Test Case", "miehe tension",
                    Patterns::Selection("miehe tension|miehe shear|symmetric bending"),
                    "Name of the test case that is to be run");
			prm.declare_entry("Timestep Size", "5e-5",
					Patterns::Double(0,1),
					"Timestep size [s]");
            prm.declare_entry("Output Skip", "1",
                    Patterns::Integer(0,-1),
                    "Generate vtk output every N steps");
            prm.declare_entry("Simulation End Time", "6.5e-3",
                    Patterns::Double(0,-1),
                    "Maxtime to run the problem till");
			prm.declare_entry("Quick Test", "false",
                    Patterns::Bool(),
                    "True causes timestep to adapt to 1e-5 near rupture");
            prm.declare_entry("Maximum Timesteps", "350",
					Patterns::Integer(0, 10000),
					"[DISABLED: Use maxtime] Number of timesteps to take");
            prm.declare_entry("Decompose Stress Matrix", "false",
                    Patterns::Bool(),
                    "True/Non-zero results in spectral decomposition of stress tensor in the matrix");
            prm.declare_entry("Decompose Stress RHS", "false",
                    Patterns::Bool(),
                    "True/Non-zero results in spectral decomposition of stress tensor in the RHS");
	}
	prm.leave_subsection();

	prm.enter_subsection("Solver Parameters"); {
			prm.declare_entry("Maximum Newton Iterations", "50", 
					Patterns::Integer(0,-1),
					"Maximum allowable Newton Iterations at a given level");
            prm.declare_entry("Newton Tolerance", "5.0e-8",
                    Patterns::Double(0, -1),
                    "Tolerance of the newton iterations goes here");
            prm.declare_entry("Penalty Gamma", "1e-15",
                    Patterns::Double(0, -1),
                    "Constant penalty term from outer loop PF-constraint");
            prm.declare_entry("Maximum Penalty Iterations", "500",
                    Patterns::Integer(0, -1),
                    "Maximum allowable augmented lagrange iterations at a given timestep");
            prm.declare_entry("Optimization Type", "simple penalization",
                    Patterns::Selection("simple penalization|augmented lagrange"),
                    "Type of outer-loop optimization to be employed");
            prm.declare_entry("AugLag Tolerance", "1.0e-4",
                    Patterns::Double(0,-1),
                    "Tolerance of the outer loop Augmented Lagrange iterations");
            prm.declare_entry("Timestep Cutting", "false", 
            		Patterns::Bool(), 
            		"True results in cutting of the timestep in the event of non-convergence");
	}
	prm.leave_subsection();
}

void InputHandler::parse_command_line(const int argc, char *const *argv) {
	if (argc <2) {
		print_usage_message ();
		exit (1);
	}

	std::list<std::string> args;
	for (int i=1; i<argc; ++i)
		args.push_back (argv[i]);

	while (args.size()) {
		if (args.front() == std::string("-p")) {
			if (args.size() == 1) {
				std::cerr << "Error: flag '-p' must be followed by the "
						<< "name of a parameter file."
						<< std::endl;
				print_usage_message ();
				exit (1);
			}
			args.pop_front ();
			const std::string parameter_file = args.front ();
			args.pop_front ();
			prm.read_input (parameter_file);

			prm.enter_subsection("Elasticity Parameters"); {
				lame_mu = prm.get_double("Lame's Parameter Mu");
				lame_lambda = prm.get_double("Lame's Parameter Lambda");
                critical_Gc = prm.get_double("Critical Fracture Energy");
			}
			prm.leave_subsection();

			prm.enter_subsection("DG Parameters"); {
				penalty_DG = prm.get_double("Face Term Penalty");
				sform = prm.get_integer("sform");
				penalty_BC = prm.get_double("Boundary Term Penalty");
                scale_penalty = prm.get_bool("Scale Penalty");
			}
			prm.leave_subsection();

			prm.enter_subsection("Phase-Field Parameters"); {
				dynamicKE = prm.get_bool("Dynamic PF Parameters");
                pf_kappa = prm.get_double("Constant K");
				pf_epsilon = prm.get_double("Constant E");
                lintype = prm.get("Linearization Scheme");
			}
			prm.leave_subsection();

			prm.enter_subsection("Mesh Parameters"); {
				ref_apriori = prm.get_bool("Refine Apriori");
				predictorcorrector = prm.get_bool("Predictor Corrector");
				global_refines = prm.get_integer("Num Global Refines");
				ref_pf_threshold = prm.get_double("Threshold PF for Refinement");
			}
			prm.leave_subsection();

			prm.enter_subsection("Runtime Parameters"); {
                testcase = prm.get("Test Case");
				timestep = prm.get_double("Timestep Size");
				max_time_steps = prm.get_integer("Maximum Timesteps");
                maxtime = prm.get_double("Simulation End Time");
                output_skip = prm.get_integer("Output Skip");
                decompose_stress_matrix = prm.get_bool("Decompose Stress Matrix");
                quicktest = prm.get_bool("Quick Test");
                decompose_stress_rhs = prm.get_bool("Decompose Stress RHS");
			}
			prm.leave_subsection();

            prm.enter_subsection("Solver Parameters"); {
                tolerance_newton = prm.get_double("Newton Tolerance");
                opttype = prm.get("Optimization Type");
                penalty_AL = prm.get_double("Penalty Gamma");
                max_iters_AL = prm.get_integer("Maximum Penalty Iterations");
                tolerance_AL = prm.get_double("AugLag Tolerance");
                max_iters_newt = prm.get_integer("Maximum Newton Iterations");
                cut_timestep = prm.get_bool("Timestep Cutting");
            }
            prm.leave_subsection();

			flag_success = true;
		}
		else {
			args.pop_front ();
		}
	}

	if (flag_success == false) {
		std::cerr << "Error: Improperly specified input file." << std::endl;
		print_usage_message ();
		exit (1);
	}
}
